package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/jrkt/go-cicd-lab/generate"
)

func main() {

	r := mux.NewRouter()
	r.HandleFunc("/greeting/{salutation}", func(w http.ResponseWriter, r *http.Request) {
		vars := mux.Vars(r)
		greeting := generate.Response(vars["salutation"])
		w.Write([]byte(greeting))
	})

	log.Println("serving on :8000")
	if err := http.ListenAndServe(":8000", r); err != nil {
		log.Fatalln("ListAndServeErr:", err)
	}
}
