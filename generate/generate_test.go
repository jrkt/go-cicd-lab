package generate_test

import (
	"testing"

	"gitlab.com/jrkt/go-cicd-lab/generate"
)

func TestResponse(t *testing.T) {
	cases := map[string]struct {
		Input          string
		ExpectedOutput string
		ExpectedPass   bool
	}{
		"Hello":  {Input: "Hello", ExpectedOutput: "Hello World", ExpectedPass: true},
		"Yoooo":  {Input: "Yoooo", ExpectedOutput: "Yoooo Gophers", ExpectedPass: true},
		"Sup":    {Input: "Sup", ExpectedOutput: "Sup Gophers", ExpectedPass: true},
		"Hola":   {Input: "Hola", ExpectedOutput: "Hola Gophers", ExpectedPass: true},
		"Helloo": {Input: "Helloo", ExpectedOutput: "Helloo World", ExpectedPass: false},
	}

	for key, tc := range cases {
		response := generate.Response(tc.Input)
		if response != tc.ExpectedOutput && tc.ExpectedPass {
			t.Fatalf("[%s] expected %s, got %s", key, tc.ExpectedOutput, response)
		}
	}
}
