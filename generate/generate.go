package generate

func Response(greeting string) string {
	if greeting == "Hello" {
		greeting += " World"
	} else {
		greeting += " Gophers"
	}

	return greeting
}
